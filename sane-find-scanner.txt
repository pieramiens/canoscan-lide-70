This is sane-find-scanner from sane-backends 1.0.18

  # sane-find-scanner will now attempt to detect your scanner. If the
  # result is different from what you expected, first make sure your
  # scanner is powered up and properly connected to your computer.

searching for SCSI scanners:
checking /dev/scanner... failed to open (Invalid argument)
checking /dev/sg0... failed to open (Access to resource has been denied)
checking /dev/sg1... failed to open (Access to resource has been denied)
checking /dev/sg2... failed to open (Access to resource has been denied)
checking /dev/sg3... failed to open (Access to resource has been denied)
checking /dev/sg4... failed to open (Access to resource has been denied)
checking /dev/sg5... failed to open (Access to resource has been denied)
checking /dev/sg6... failed to open (Access to resource has been denied)
checking /dev/sg7... failed to open (Access to resource has been denied)
checking /dev/sg8... failed to open (Access to resource has been denied)
checking /dev/sg9... failed to open (Access to resource has been denied)
checking /dev/sga... failed to open (Invalid argument)
checking /dev/sgb... failed to open (Invalid argument)
checking /dev/sgc... failed to open (Invalid argument)
checking /dev/sgd... failed to open (Invalid argument)
checking /dev/sge... failed to open (Invalid argument)
checking /dev/sgf... failed to open (Invalid argument)
checking /dev/sgg... failed to open (Invalid argument)
checking /dev/sgh... failed to open (Invalid argument)
checking /dev/sgi... failed to open (Invalid argument)
checking /dev/sgj... failed to open (Invalid argument)
checking /dev/sgk... failed to open (Invalid argument)
checking /dev/sgl... failed to open (Invalid argument)
checking /dev/sgm... failed to open (Invalid argument)
checking /dev/sgn... failed to open (Invalid argument)
checking /dev/sgo... failed to open (Invalid argument)
checking /dev/sgp... failed to open (Invalid argument)
checking /dev/sgq... failed to open (Invalid argument)
checking /dev/sgr... failed to open (Invalid argument)
checking /dev/sgs... failed to open (Invalid argument)
checking /dev/sgt... failed to open (Invalid argument)
checking /dev/sgu... failed to open (Invalid argument)
checking /dev/sgv... failed to open (Invalid argument)
checking /dev/sgw... failed to open (Invalid argument)
checking /dev/sgx... failed to open (Invalid argument)
checking /dev/sgy... failed to open (Invalid argument)
checking /dev/sgz... failed to open (Invalid argument)
  # No SCSI scanners found. If you expected something different, make sure that
  # you have loaded a kernel SCSI driver for your SCSI adapter.

searching for USB scanners:
checking /dev/usb/scanner... failed to open (Invalid argument)
checking /dev/usb/scanner0... failed to open (Invalid argument)
checking /dev/usb/scanner1... failed to open (Invalid argument)
checking /dev/usb/scanner2... failed to open (Invalid argument)
checking /dev/usb/scanner3... failed to open (Invalid argument)
checking /dev/usb/scanner4... failed to open (Invalid argument)
checking /dev/usb/scanner5... failed to open (Invalid argument)
checking /dev/usb/scanner5... failed to open (Invalid argument)
checking /dev/usb/scanner7... failed to open (Invalid argument)
checking /dev/usb/scanner8... failed to open (Invalid argument)
checking /dev/usb/scanner9... failed to open (Invalid argument)
checking /dev/usb/scanner10... failed to open (Invalid argument)
checking /dev/usb/scanner11... failed to open (Invalid argument)
checking /dev/usb/scanner12... failed to open (Invalid argument)
checking /dev/usb/scanner13... failed to open (Invalid argument)
checking /dev/usb/scanner14... failed to open (Invalid argument)
checking /dev/usb/scanner15... failed to open (Invalid argument)
checking /dev/usbscanner... failed to open (Invalid argument)
checking /dev/usbscanner0... failed to open (Invalid argument)
checking /dev/usbscanner1... failed to open (Invalid argument)
checking /dev/usbscanner2... failed to open (Invalid argument)
checking /dev/usbscanner3... failed to open (Invalid argument)
checking /dev/usbscanner4... failed to open (Invalid argument)
checking /dev/usbscanner5... failed to open (Invalid argument)
checking /dev/usbscanner6... failed to open (Invalid argument)
checking /dev/usbscanner7... failed to open (Invalid argument)
checking /dev/usbscanner8... failed to open (Invalid argument)
checking /dev/usbscanner9... failed to open (Invalid argument)
checking /dev/usbscanner10... failed to open (Invalid argument)
checking /dev/usbscanner11... failed to open (Invalid argument)
checking /dev/usbscanner12... failed to open (Invalid argument)
checking /dev/usbscanner13... failed to open (Invalid argument)
checking /dev/usbscanner14... failed to open (Invalid argument)
checking /dev/usbscanner15... failed to open (Invalid argument)
trying libusb:

<device descriptor of 0x0000/0x0000 at 003:001>
bLength               18
bDescriptorType       1
bcdUSB                1.10
bDeviceClass          9
bDeviceSubClass       0
bDeviceProtocol       0
bMaxPacketSize0       8
idVendor              0x0000
idProduct             0x0000
bcdDevice             2.06
iManufacturer         3 ((null))
iProduct              2 ((null))
iSerialNumber         1 ((null))
bNumConfigurations    1
 <configuration 0>
 bLength              9
 bDescriptorType      2
 wTotalLength         25
 bNumInterfaces       1
 bConfigurationValue  1
 iConfiguration       0 ()
 bmAttributes         192 (Self-powered)
 MaxPower             0 mA
  <interface 0>
   <altsetting 0>
   bLength            9
   bDescriptorType    4
   bInterfaceNumber   0
   bAlternateSetting  0
   bNumEndpoints      1
   bInterfaceClass    9
   bInterfaceSubClass 0
   bInterfaceProtocol 0
   iInterface         0 ()
    <endpoint 0>
    bLength           7
    bDescriptorType   5
    bEndpointAddress  0x81 (in 0x01)
    bmAttributes      3 (interrupt)
    wMaxPacketSize    2
    bInterval         255 ms
    bRefresh          0
    bSynchAddress     0

<device descriptor of 0x04f2/0x0116 at 002:003>
bLength               18
bDescriptorType       1
bcdUSB                1.10
bDeviceClass          0
bDeviceSubClass       0
bDeviceProtocol       0
bMaxPacketSize0       8
idVendor              0x04F2
idProduct             0x0116
bcdDevice             3.00
iManufacturer         1 ((null))
iProduct              2 ((null))
iSerialNumber         0 ()
bNumConfigurations    1
 <configuration 0>
 bLength              9
 bDescriptorType      2
 wTotalLength         34
 bNumInterfaces       1
 bConfigurationValue  1
 iConfiguration       0 ()
 bmAttributes         160 (Remote Wakeup)
 MaxPower             100 mA
  <interface 0>
   <altsetting 0>
   bLength            9
   bDescriptorType    4
   bInterfaceNumber   0
   bAlternateSetting  0
   bNumEndpoints      1
   bInterfaceClass    3
   bInterfaceSubClass 1
   bInterfaceProtocol 1
   iInterface         0 ()
    <endpoint 0>
    bLength           7
    bDescriptorType   5
    bEndpointAddress  0x81 (in 0x01)
    bmAttributes      3 (interrupt)
    wMaxPacketSize    8
    bInterval         10 ms
    bRefresh          0
    bSynchAddress     0

<device descriptor of 0x045e/0x0029 at 002:002>
bLength               18
bDescriptorType       1
bcdUSB                1.10
bDeviceClass          0
bDeviceSubClass       0
bDeviceProtocol       0
bMaxPacketSize0       8
idVendor              0x045E
idProduct             0x0029
bcdDevice             1.08
iManufacturer         1 ((null))
iProduct              2 ((null))
iSerialNumber         0 ()
bNumConfigurations    1
 <configuration 0>
 bLength              9
 bDescriptorType      2
 wTotalLength         34
 bNumInterfaces       1
 bConfigurationValue  1
 iConfiguration       0 ()
 bmAttributes         160 (Remote Wakeup)
 MaxPower             100 mA
  <interface 0>
   <altsetting 0>
   bLength            9
   bDescriptorType    4
   bInterfaceNumber   0
   bAlternateSetting  0
   bNumEndpoints      1
   bInterfaceClass    3
   bInterfaceSubClass 1
   bInterfaceProtocol 2
   iInterface         0 ()
    <endpoint 0>
    bLength           7
    bDescriptorType   5
    bEndpointAddress  0x81 (in 0x01)
    bmAttributes      3 (interrupt)
    wMaxPacketSize    4
    bInterval         10 ms
    bRefresh          0
    bSynchAddress     0

<device descriptor of 0x0000/0x0000 at 002:001>
bLength               18
bDescriptorType       1
bcdUSB                1.10
bDeviceClass          9
bDeviceSubClass       0
bDeviceProtocol       0
bMaxPacketSize0       8
idVendor              0x0000
idProduct             0x0000
bcdDevice             2.06
iManufacturer         3 ((null))
iProduct              2 ((null))
iSerialNumber         1 ((null))
bNumConfigurations    1
 <configuration 0>
 bLength              9
 bDescriptorType      2
 wTotalLength         25
 bNumInterfaces       1
 bConfigurationValue  1
 iConfiguration       0 ()
 bmAttributes         192 (Self-powered)
 MaxPower             0 mA
  <interface 0>
   <altsetting 0>
   bLength            9
   bDescriptorType    4
   bInterfaceNumber   0
   bAlternateSetting  0
   bNumEndpoints      1
   bInterfaceClass    9
   bInterfaceSubClass 0
   bInterfaceProtocol 0
   iInterface         0 ()
    <endpoint 0>
    bLength           7
    bDescriptorType   5
    bEndpointAddress  0x81 (in 0x01)
    bmAttributes      3 (interrupt)
    wMaxPacketSize    2
    bInterval         255 ms
    bRefresh          0
    bSynchAddress     0

<device descriptor of 0x04a9/0x2224 at 001:006 (Canon CanoScan)>
bLength               18
bDescriptorType       1
bcdUSB                2.00
bDeviceClass          255
bDeviceSubClass       255
bDeviceProtocol       255
bMaxPacketSize0       64
idVendor              0x04A9
idProduct             0x2224
bcdDevice             0.00
iManufacturer         1 (Canon)
iProduct              2 (CanoScan)
iSerialNumber         0 ()
bNumConfigurations    1
 <configuration 0>
 bLength              9
 bDescriptorType      2
 wTotalLength         32
 bNumInterfaces       1
 bConfigurationValue  1
 iConfiguration       0 ()
 bmAttributes         160 (Remote Wakeup)
 MaxPower             500 mA
  <interface 0>
   <altsetting 0>
   bLength            9
   bDescriptorType    4
   bInterfaceNumber   0
   bAlternateSetting  0
   bNumEndpoints      2
   bInterfaceClass    255
   bInterfaceSubClass 255
   bInterfaceProtocol 255
   iInterface         0 ()
    <endpoint 0>
    bLength           7
    bDescriptorType   5
    bEndpointAddress  0x02 (out 0x02)
    bmAttributes      2 (bulk)
    wMaxPacketSize    512
    bInterval         0 ms
    bRefresh          0
    bSynchAddress     0
    <endpoint 1>
    bLength           7
    bDescriptorType   5
    bEndpointAddress  0x83 (in 0x03)
    bmAttributes      2 (bulk)
    wMaxPacketSize    512
    bInterval         0 ms
    bRefresh          0
    bSynchAddress     0

<trying to find out which USB chip is used>
    checking for GT-6801 ...
    this is not a GT-6801 (bcdUSB = 0x200)
    checking for GT-6816 ...
    this is not a GT-6816 (bDeviceClass = 255, bInterfaceClass = 255)
    checking for GT-8911 ...
    this is not a GT-8911 (check 1, bDeviceClass = 255, bInterfaceClass = 255)
    checking for MA-1017 ...
    this is not a MA-1017 (bDeviceClass = 255, bInterfaceClass = 255)
    checking for MA-1015 ...
    this is not a MA-1015 (bcdUSB = 0x200)
    checking for MA-1509 ...
    this is not a MA-1509 (bcdUSB = 0x200)
    checking for LM983[1,2,3] ...
    this is not a LM983x (bcdUSB = 0x200)
    checking for GL646 ...
    this is not a GL646 (bDeviceClass = 255, bInterfaceClass = 255)
    checking for GL646_HP ...
    this is not a GL646_HP (bcdUSB = 0x200)
    checking for GL660+GL646 ...
    this is not a GL660+GL646 (bDeviceClass = 255, bInterfaceClass = 255)
    checking for GL841 ...
    this is not a GL841 (bNumEndpoints = 2)
    checking for ICM532B ...
    this is not a ICM532B (check 2, bcdUSB = 0x200)
    checking for PV8630/LM9830 ...
    this is not a PV8630/LM9830 (bDeviceClass = 255)
    checking for M011 ...
    this is not a M011 (bcdUSB = 0x200)
    checking for RTS8822L-01H ...
    this is not a RTS8822L-01H (bDeviceClass = 255)
    checking for rts8858c ...
    this is not a rts8858c (bDeviceClass = 255)
    checking for SQ113 ...
    this is not a SQ113 (bDeviceClass = 255)
<Couldn't determine the type of the USB chip (result from sane-backends 1.0.18)>

found USB scanner (vendor=0x04a9 [Canon], product=0x2224 [CanoScan]) at libusb:001:006

<device descriptor of 0x0000/0x0000 at 001:001>
bLength               18
bDescriptorType       1
bcdUSB                2.00
bDeviceClass          9
bDeviceSubClass       0
bDeviceProtocol       1
bMaxPacketSize0       8
idVendor              0x0000
idProduct             0x0000
bcdDevice             2.06
iManufacturer         3 ((null))
iProduct              2 ((null))
iSerialNumber         1 ((null))
bNumConfigurations    1
 <configuration 0>
 bLength              9
 bDescriptorType      2
 wTotalLength         25
 bNumInterfaces       1
 bConfigurationValue  1
 iConfiguration       0 ()
 bmAttributes         224 (Self-poweredRemote Wakeup)
 MaxPower             0 mA
  <interface 0>
   <altsetting 0>
   bLength            9
   bDescriptorType    4
   bInterfaceNumber   0
   bAlternateSetting  0
   bNumEndpoints      1
   bInterfaceClass    9
   bInterfaceSubClass 0
   bInterfaceProtocol 0
   iInterface         0 ()
    <endpoint 0>
    bLength           7
    bDescriptorType   5
    bEndpointAddress  0x81 (in 0x01)
    bmAttributes      3 (interrupt)
    wMaxPacketSize    2
    bInterval         12 ms
    bRefresh          0
    bSynchAddress     0
  # Your USB scanner was (probably) detected. It may or may not be supported by
  # SANE. Try scanimage -L and read the backend's manpage.

  # Not checking for parallel port scanners.

  # Most Scanners connected to the parallel port or other proprietary ports
  # can't be detected by this program.

  # You may want to run this program as root to find all devices. Once you
  # found the scanner devices, be sure to adjust access permissions as
  # necessary.
done
