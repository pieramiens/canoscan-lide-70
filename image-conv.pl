#!/usr/bin/perl

$file = shift ( @ARGV ) ;
$x_max = shift ( @ARGV ) ;
$y_max = shift ( @ARGV ) ;

unless ( $x_max )
{
  $x_max = 128 ;
}

unless ( $y_max )
{
  $y_max = 128 ;
}

open ( INDAT , '<' . $file ) ;

open ( OUTDAT , '>' . $file . '.pnm' ) ;
print OUTDAT 'P6' , "\n" ;
print OUTDAT $x_max , ' ' , $y_max , "\n" ;
print OUTDAT '255' , "\n" ;

my $y = 0 ;
my $x = 0 ;
my @dataR = ( ) ;
my @dataG = ( ) ;
my @dataB = ( ) ;
my $state = 'R' ;
my @data = ( ) ;

while ( !eof(INDAT) )
{
  unless ( @data )
  {
    my $zeile = <INDAT> ;
    @data = split ( m!\s+! , $zeile ) ;
  }

  if ( @data && ( $state eq 'R' ) )
  {
    my $len = $x_max - @dataR ;

    if ( @data < $len )
    {
      push ( @dataR , @data ) ;
      @data = ( ) ;
    }
    else
    {
      my @part = splice ( @data , 0 , $len ) ;
      push ( @dataR , @part ) ;
      $state = 'G' ;
    }
  }

  if ( @data && ( $state eq 'G' ) )
  {
    my $len = $x_max - @dataG ;

    if ( @data < $len )
    {
      push ( @dataG , @data ) ;
      @data = ( ) ;
    }
    else
    {
      my @part = splice ( @data , 0 , $len ) ;
      push ( @dataG , @part ) ;
      $state = 'B' ;
    }
  }

  if ( @data && ( $state eq 'B' ) )
  {
    my $len = $x_max - @dataB ;

    if ( @data < $len )
    {
      push ( @dataB , @data ) ;
      @data = ( ) ;
    }
    else
    {
      my @part = splice ( @data , 0 , $len ) ;
      push ( @dataB , @part ) ;
      $state = 'STORE' ;
    }
  }

  if ( $state eq 'STORE' )
  {
    for ( $x = 0 ; $x < $x_max ; $x ++ )
    {
      my $rgb = pack ( 'ccc' , hex($dataR[$x]) , hex($dataG[$x]) , hex($dataB[$x]) ) ;
      print OUTDAT $rgb ;
    }

    $y ++ ;
    @dataR = ( ) ;
    @dataG = ( ) ;
    @dataB = ( ) ;
    $state = 'R' ;
  }
}

close ( OUTDAT ) ;
close ( INDAT ) ;
