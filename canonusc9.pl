#!/usr/bin/perl
use Device::USB ;

my $timeout = 500 ;
my $buffer = "\0" x 512 ;
my $usb = Device::USB -> new() ;
my $dev = $usb -> find_device ( 0x04a9 , 0x2225 ) ;
$dev -> open() ;
$dev -> claim_interface() ;
print 'libusb:xxx:' , $dev -> filename() , "\n" ;
$backstarttime = time() ;

@data = control_msg ( $dev , '80 06 00 01 00 00 12 00' ) ;
printhex ( @data ) ;

read_snif( 'usbsnoop7005.txt' , 1801 , 2041 ) ;
my $hex46;
while ( 1 )
{
  cp2155_get ( 0x46 , $value ) ;
  $hex46 = bin2hex ( $value );
  unless ( $hex46 == "33 35" || $hex46 == "37") {
    print $hex46." - " ;
    }
# goes from 33 to 36 37 to 33 35 to 34 33 to 31 31 to 38
#  if ( bin2hex ( $value ) == "34 33" || bin2hex ( $value ) == "31 31"
#     || bin2hex ( $value ) == "33" )
#  if ( bin2hex ( $value ) == "33" )
  if ( $hex46 == "34 33" || $hex46  == "31 31" )
  {
# stop motor:
    cp2155_set( 0x80 , 0x12 ) ;
    cp2155_set( 0x11 , 0x25 ) ; # 25 or 83
    cp2155_set( 0x9b , 0x01 ) ;
    read_snif( 'usbsnoop7006.txt' , 6 , 1340 ) ;
    printf ( "\ngoing back took %ld seconds\n", time()-$backstarttime ) ;
    exit 0 ;
#    last ;
  }
}
=begin comment
read_snif( 'motor1.txt' , 6 , 825 ) ;
while ( 1 )
{
  cp2155_get(0x46,$value) ;
  print bin2hex($value).' ' ;
  if ( $value == 8 || $value == 0x38)
  {
    last ;
  }
}
cp2155_set( 0x80 , 0x12 ) ;
cp2155_set( 0x11 , 0x83 ) ;
read_snif( 'motor2.txt' , 834 , 1081 ) ;
while ( 1 )
{
  cp2155_get(0x46,$value) ;
  print bin2hex($value).' ' ;
  if ( $value == 0 || $value == 0x38)
  {
    last ;
  }
}
cp2155_set( 0x80 , 0x12 ) ;
cp2155_set( 0x11 , 0x83 ) ;
1 ;
=end comment
=cut
sub read_snif
{
  my ($file, $urb_start, $urb_ende ) = @_ ;
  open ( INDAT , '<' . $file ) ;
  while ( !eof ( INDAT ) )
  {
    ( $epD , $dataD , $infoD , $epB , $dataB , $infoB ) = get_urb_data() ;
    my @urb = split ( m![\(\)\s]+! , $infoD ) ;

    if ( $urb[3] eq 'URB' )
    {
      if ( $urb_start && ( $urb[4] < $urb_start ) )
      {
        next ( ) ;
      } 

      if ( $urb_ende && ( $urb[4] > $urb_ende ) )
      {
        last ( ) ;
      }
    }

    print $infoD , "\n" ;
    my $cnt = $dev -> bulk_write ( hex($epD) , hex2bin($dataD) , $timeout ) ;

    if ( ( $cnt < 0 ) && ( $epD != '03' ) )
    {
      error ( 'BULK > 0x' . $epD . ' returned error code ' . $cnt ) ;
      print 'error: ' , $cnt , "\n" ;
    }

    if ( $epB )
    {
      print $infoB ;
      my $data = ' ' ;
      my $cnt = $dev -> bulk_read ( hex($epB) , $data , 1 , $timeout ) ;

      if ( $cnt < 0 )
      {
        print "\n" ;
        error ( 'BULK < 0x' . $epB . ' returned error code ' . $cnt ) ;
      }

      $data = bin2hex ( $data ) ;
      print ' ret=' , $data , "\n" ;
    }
  }
  close ( INDAT ) ;
}
sub get_urb_data
{
  my $state = 'INFO_DOWN' ;
  my ( $typD , $dirD , $epD , $dataD , $infoD ) = ( ) ;
  my ( $typB , $dirB , $epB , $dataB , $infoB ) = ( ) ;

  while ( $zeile = <INDAT> )
  {
    chomp ( $zeile ) ;

    if ( substr ( $zeile , 0 , 1 ) eq '#' )
    {
      next ( ) ;
    }

    if ( $state eq 'INFO_DOWN' )
    {
      unless ( $zeile )
      {
        next ( ) ;
      }

      ( $typD , $dirD , $epD ) = split ( m! +! , $zeile ) ;
      $infoD = $zeile ;

      unless ( $typD eq 'BULK' )
      {
        error ( 'Type does not match BULK' , $zeile ) ;
      }

      $state = 'DATA_DOWN' ;
      $dataD = '' ;
    }
    elsif ( $state eq 'DATA_DOWN' )
    {
      if ( substr ( $zeile , 12 , 1 ) eq ':' )
      {
        $dataD .= $zeile ;
      }
      elsif ( $zeile )
      {
        ( $typB , $dirB , $epB ) = split ( m! +! , $zeile ) ;
        $infoB = $zeile ;

        unless ( $typB eq 'BULK' )
        {
          error ( 'Type does not match BULK' , $infoD , $zeile ) ;
        }

        $state = 'DATA_BACK' ;
        $dataB = '' ;
      }
      else
      {
        last ( ) ;
      }
    }
    elsif ( $state eq 'DATA_BACK' )
    {
      if ( substr ( $zeile , 12 , 1 ) eq ':' )
      {
        $dataB .= $zeile ;
      }
      elsif ( $zeile )
      {
        error ( 'Empty line expected' , $infoD , $infoB , $zeile ) ;
      }
      else
      {
        last ( ) ;
      }
    }
    else
    {
      error ( 'Illegal state' , $infoD , $zeile ) ;
    }
  }

  $dataD = dez2hex ( hex2dez ( $dataD ) ) ;
  $dataB = dez2hex ( hex2dez ( $dataB ) ) ;
  return ( $epD , $dataD , $infoD , $epB , $dataB , $infoB ) ;
}

sub bin2dez
{
  my ( $data ) = @_ ;
  my @data = unpack ( 'C*' , $data ) ;
  return ( @data )
}

sub dez2bin
{
  my ( @data ) = @_ ;
  my $data = pack ( 'C*' , @data ) ;
  return ( $data )
}

sub hex2dez
{
  my ( $txt ) = @_ ;
  $txt =~ s![\da-f]+:!!ig ;
  $txt =~ s!\A\s+!! ;
  $txt =~ s!\s+\Z!! ;
  my @data = split ( m!\s+! , $txt ) ;

  foreach ( @data )
  {
    $_ = hex ( $_ ) ;
  }

  return ( @data ) ;
}

sub dez2hex
{
  my ( @line ) = @_ ;
  my $anz = @line ;
  my $line = sprintf ( '%02x ' x $anz , @line ) ;
  return ( $line ) ;
}

sub bin2hex
{
  my ( $data ) = @_ ;
  my @data = bin2dez ( $data ) ;
  my $txt = dez2hex ( @data ) ;
  return ( $txt ) ;
}

sub hex2bin
{
  my ( $txt ) = @_ ;
  my @data = hex2dez ( $txt ) ;
  my $data = dez2bin ( @data ) ;
  return ( $data ) ;
}

sub control_msg
{
  my ( $dev , $txt ) = @_ ;
  my ( $typ , $req , $Lv , $Hv , $Li , $Hi , $Ls , $Hs ) = hex2dez ( $txt ) ;
  my $value = $Hv*256 + $Lv ;
  my $index = $Hi*256 + $Li ;
  my $size = $Hs*256 + $Ls ;
  my $buffer = "\0" x $size ;
  my $retval = $dev -> control_msg ( $typ , $req , $value , $index , $buffer , $size , $timeout ) ;
  my @buffer = bin2dez ( $buffer ) ;
  return ( @buffer ) ;
}

sub printhex
{
  my ( @data ) = @_ ;

  while ( @data )
  {
    my @line = splice ( @data , 0 , 16 ) ;
    my $line = dez2hex ( @line ) ;
    print $line , "\n" ;
  }
}

sub display
{
  my ( $dev , $key , $val , $txt ) = @_ ;
  my $pad = ' ' x 22 ;
  $key = substr ( $key . $pad , 0 , 22 ) ;

  if ( $txt == 1 )
  {
    $txt = $dev -> get_string_simple ( $val ) ;
  }

  if ( $txt )
  {
    $txt = ' (' . $txt . ')' ;
  }

  print ( $key , $val , $txt , "\n" ) ;
}

sub error
{
  my ( @msg ) = @_ ;
  $msg[0] .= ' !' ;
  print 'ERROR: ' , join ( "\n" , @msg ) , "\n" ;
  exit ;
}
#*****************************************************
#            CP2155 communication primitives
#   Provides I/O routines to Philips CP2155BE chip
#*****************************************************

# Write single byte to CP2155 register

sub cp2155_set
{
  my ( $reg , $val ) = @_ ;
  my @data = ( ($reg >> 8) & 0xff , ($reg & 0xff) , 0x01 , 0x00 , $val ) ;
  my $status = $dev -> bulk_write ( 0x02 , dez2bin(@data) , $timeout ) ;

  if ( $status < 0 )
  {
    DBG ( 1 , "cp2155_set: sanei_usb_write_bulk error\n" ) ;
    return ( 'SANE_STATUS_IO_ERROR' ) ;
  }

  return ( 'SANE_STATUS_GOOD' ) ;
}

# Read single byte from CP2155 register
sub cp2155_get
{
  my $reg = $_[0] ;
  my @data = ( 0x01 , $reg , 0x01 , 0x00 ) ;
  my $status = $dev -> bulk_write ( 0x02 , dez2bin(@data) , $timeout ) ;

  if ( $status < 0 )
  {
    DBG ( 1 , "cp2155_get: sanei_usb_write_bulk error\n" ) ;
    return ( 'SANE_STATUS_IO_ERROR' ) ;
  }

 usleep ( 0.001 ) ;

  my $data = ' ' ;
  my $status = $dev -> bulk_read ( 0x83 , $data , 1 , $timeout ) ;
  $_[1] = ord ( $data ) ;

  if ( $status < 0 )
  {
    DBG ( 1 , "cp2155_get: sanei_usb_read_bulk error\n" ) ;
    return ( 'SANE_STATUS_IO_ERROR' ) ;
  }

  return ( 'SANE_STATUS_GOOD' ) ;
}

sub usleep
{
  my ( $time ) = @_ ;
  select ( undef , undef , undef , $time ) ;
}

